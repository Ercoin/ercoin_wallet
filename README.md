# Ercoin wallet

A simple wallet which may be used to experiment with [Ercoin](https://gitlab.com/Ercoin/ercoin).

This wallet is **obsolete**; as it doesn’t support at least the following:

* intented validator public keys in locked accounts;
* timestamps used for time measurement instead of block height (this can be bypassed, as both use the same format);
* new RPC port.

## Development installation

1. Install and run Ercoin.
2. `make deps`.

## Usage

Use one of the following commands:
```sh
./wallet get_account <address>
./walet extend_account <from_address> <to_address> <valid_until>
./wallet lock_account <address> <locked_until>
./wallet gen_keypair
./wallet current_height
./wallet send <from_address> <to_address> <amount> <message>
```

Keys are stored in the `private_keys` file as hex-encoded public and private keys, separated by spaces, one key pair per line.

## License

This software is licensed under under [the Apache License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0) (the “License”); you may not use this software except in compliance with the License. Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an “AS IS” BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the specific language governing permissions and limitations under the License.
